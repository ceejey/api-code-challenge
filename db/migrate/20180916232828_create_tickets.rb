class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :request_number
      t.string :sequence_number
      t.string :request_type
      t.datetime :request_taken
      t.datetime :transmission_date_time
      t.datetime :legal
      t.datetime :response_due
      t.date :restake_on
      t.date :expiration_on
      t.string :primary_service_area_code
      t.text :additional_service_area_codes
      t.text :digsiteInfo

      t.timestamps
    end
  end
end
