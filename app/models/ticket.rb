class Ticket < ApplicationRecord
	has_many :excavators
	serialize :additional_service_area_codes, Array
end
