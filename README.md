# API code challenge

A code challenge as a technical test.

Ruby : ruby-2.5.1
Rails : '~> 5.2.1'
App Setup:
db:create
db:migrate
db:seed
(OR db:setup)
