class Api::V1::BaseController < ActionController::API

  # before_action :authenticate_user!, except: :options # If session needs.

  rescue_from ActionController::ParameterMissing do
    api_error(status: 400, errors: 'Invalid parameters')
  end

  rescue_from ActiveRecord::RecordNotFound do
    api_error(status: 404, errors: 'Resource not found!')
  end

  # rescue_from UnauthenticatedError do
  #   unauthenticated!
  # end

  protected

    def paginate(resource)
      default_per_page = Rails.application.secrets.default_per_page || 25

      resource.paginate({
        page: params[:page] || 1, per_page: params[:per_page] || default_per_page
      })
    end

    def api_error(status: 500, errors: [])
      puts errors.full_messages if errors.respond_to?(:full_messages)

      render json: Api::V1::ErrorSerializer.new(status, errors).as_json,
        status: status
    end
end
