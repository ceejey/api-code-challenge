Rails.application.routes.draw do
  #root 'v1/tickets#index'

  # api routes go here!
  namespace :api do
    namespace :v1 do
      #resources :sessions, only: [:create, :show] -- If session needs.
      resources :tickets, only: [:index, :create, :show, :update, :destroy] do
    		resources :excavators do
          # post/get actions goes here!
        end
  		end      
    end
  end
end